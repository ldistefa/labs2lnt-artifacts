module BOIDS_PARALLEL_MATRIX is

function MAXCOMPONENTS : nat is return 3 end function

type ID is X : nat where X < MAXCOMPONENTS with ==, != end type
type PC is array [0 .. 0] of nat end type
type Iface is array [0 .. 1] of int with get, set end type

channel IDchannel is (ID) end channel

type Lstig is array [0 .. 1] of Int with get, set end type
type Pending is array [0 .. 1] of bool with get, set end type


type setOfAgents is array [0 .. 2 (*MAXCOMPONENTS-1*)] of Bool with get,set end type

function length (p : Pending) : nat is
   var n, j : nat in
      n := 0;
      for j := 0 while j < 2 by j := j + 1 loop
         if p[j] then n := n + 1 end if
      end loop;
      return n
   end var
end function

function empty (p : Pending) : bool is
   return length (p) == 0
end function

function insert (k : Nat, p : Pending) : Pending is
   var p1 : Pending in
      p1 := p;
      p1[k] := true;
      return p1
   end var
end function

function remove (k : Nat, p : Pending) : Pending is
   var p1 : Pending in
      p1 := p;
      p1[k] := false;
      return p1
   end var
end function

function member (k : Nat, p : Pending) : bool is
   return p[k]
end function

function incr (x : nat) : nat is 
   if x < 255 then return x + 1 else return 0 end if
end function

function TUPLESTART (n : Nat) : Nat is
   case n in
      0 -> return 0
   |  1 -> return 0
   |  any -> raise unexpected (* should never match *)
   end case
end function

function TUPLEEND (n : Nat) : Nat is
   case n in
      0 -> return 1
   |  1 -> return 1
   |  any -> raise unexpected (* should never match *)
   end case
end function

function link (I1, I2 : Iface) : bool is
   var x0, x1 : int in
      x0 := I1[0] - I2[0];
      x1 := I1[1] - I2[1];
      return ((x0 ** 2) + (x1 ** 2)) <= (5 * 5)
   end var
end function

process sched [tick: IDchannel] is
   var t : ID in
      t := ID(0);
      loop
      select 
        null
        []
        tick (t); tick(?any ID);
        t := ID((Nat(t)+1) mod MAXCOMPONENTS)
      end select
      end loop
   end var
end process

type Comparison is LESS, SAME, GREATER with ==, != end type

function inv(c:Comparison): Comparison is
    case c in
    LESS -> return GREATER
    | GREATER -> return LESS
    | SAME -> return SAME
    end case 
end function

-- n*(n+1) / 2
type MatrixVar is array [0 .. 2] of Comparison with get, set end type
-- one Matrix2D for each variable
type Matrix is array [0 .. 1] of MatrixVar with get, set end type

function tri(n:Nat): Nat is
    -- this requirement is just to avoid complaints from LOTOS compiler
    --require n <= 5;
    (* returns the n-th triangular number *)
    return (n * (n + 1)) div 2
end function

function index(i,j:Nat): Nat is
    require j < MAXCOMPONENTS;
    require i < j;
    -- I don't know why, but writing the formula in one line, i.e.,
    -- return (MAXCOMPONENTS * i) + j - tri(i+1)
    -- leads to "out of range" errors from the LOTOS compiler
    var x:Nat in
        x := tri(i+1);
        x := (MAXCOMPONENTS * i) + j - x;
        return x
    end var
end function


--getComparison(m,i,j) = GREATER iff i's value is fresher than j's
function getComparison(m:Matrix, key,i,j:Nat): Comparison is
    if (i==j) then return SAME
    elsif (i<j) then return m[TUPLESTART(key)][index(i,j)]
    else return inv(m[TUPLESTART(key)][index(j,i)])
    end if
end function

--Enforces that a future call to getComparison(..., i,j) will return "cmp"
function setComparison(in out mkey:MatrixVar, i,j:Nat, cmp:Comparison) is
    if (i<j) then mkey[index(i,j)] := cmp
    elsif (i>j) then mkey[index(j,i)] := inv(cmp)
    -- ignore the case i=j
    end if
end function

function refresh(in out m: Matrix, key, id: Nat) is
    var i:Nat, mkey:MatrixVar in 
        mkey := m[TUPLESTART(key)];
        for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
            setComparison(!?mkey, id, i, GREATER)
            --if (id<i) then mkey[index(id, i)] := GREATER
            --elsif (id>i) then mkey[index(i, id)] := LESS
            
            --end if
        end loop;
        m[TUPLESTART(key)] := mkey
    end var
end function

function sync1(in out m: Matrix, key, from, to: Nat) is
  var i:Nat, mkey: MatrixVar in
    mkey := m[TUPLESTART(key)];
    setComparison(!?mkey, from, to, SAME);
    for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
      if (i <> from) and (i<>to) then
        setComparison(!?mkey, to, i, getComparison(m, key, from, i))
      end if
    end loop;
    m[TUPLESTART(key)]:= mkey
  end var
end function

function sync(in out m: Matrix, key, from: Nat, to: SetOfAgents) is
    var i, j:Nat, mkey: MatrixVar in 
        mkey := m[TUPLESTART(key)];
        for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
            if to[i] then 
                (* Sync receiver with sender *)
                setComparison(!?mkey, from, i, SAME);
                for j := 0 while j < i by j := j + 1 loop
                    (* Sync receivers with each other *)
                    if to[j] then setComparison(!?mkey, i, j, SAME) end if
                end loop
            elsif i <> from then
                -- Update non-receivers
                for j := 0 while j < i by j := j + 1 loop
                    (*j's comparison to "i" is the same as with "from" *)
                    if to[j]
                    then setComparison(!?mkey, i, j, getComparison(m, key, from, j))
                    end if
                end loop
            end if
            -- ignore case i = from
        end loop;
        m[TUPLESTART(key)]:= mkey
    end var
end function


process MatrixStorage[refresh, request, debug: any] is
  access debug;
  var M: Matrix, sender, receiver:ID, key:Nat, cmp:Comparison in 
    M := Matrix(MatrixVar(SAME));
    loop
      --debug(M);
      select
        refresh(?key, ?sender) where (key < 2);
        eval refresh(!?M, key, Nat(sender))
        []
        --always offer all values of M
        key := any Nat where (key < 2);
        sender := any ID;
        receiver := any ID where (receiver != sender);
        cmp := getComparison(M, key, Nat(sender), Nat(receiver));
        request(key, sender, receiver, cmp);
        -- if you offered GREATER and it was accepted,
        -- then synchronize sender and receiver
        if cmp == GREATER
        then eval sync1(!?M, key, Nat(sender), Nat(receiver))
        end if 
      end select
    end loop
  end var
end process

process goodLstig(in out l:Lstig) is
  var l0, l1 : Int in
    l0 := any int where (l0 == -1) or (l0 == 1);
    l1 := any int where (l1 == -1) or (l1 == 1);
    l[0] := l0;
    l[1] := l1
  end var
end process

process goodIface(in out i:Iface) is
  var i0, i1 : Int in
    i0 := any int where (i0 >= 0) and (i0 < 5);
    i1 := any int where (i1 >= 0) and (i1 < 5);
    i[0] := i0;
    i[1] := i1
  end var
end process


process agent [tick:IDchannel, put, get1, get2, req, resp1, resp2, init, attr, l, refresh, request : any] (id : ID) is
  var I : Iface, L : Lstig, Zput, Zqry : Pending, pc : PC, t : ID, x : int in
    -- initializations
    --tick(?t) where t==id;

    I := Iface (0);
    L := Lstig (0);
    Zput := Pending (false);
    Zqry := Pending (false);
    pc := PC (2);

    x := any int where (x == -1) or (x == 1);
    L[0] := x;
    l (id, 0 of Nat, x);
    refresh(0 of Nat, id);

    x := any int where (x == -1) or (x == 1);
    L[1] := x;
    l (id, 1 of Nat, x);
    refresh(1 of Nat, id);

    x := any int where (x >= 0) and (x < 5);
    I[0] := x;
    attr (id, 0 of Nat, x);
    x := any int where (x >= 0) and (x < 5);
    I[0] := x;
    attr (id, 1 of Nat, x);
      
    init;

    loop
      select
      only if empty (Zput) and empty (Zqry) then
        tick(?t) where (Nat(t) < MAXCOMPONENTS);
        if (t == id) then
          -- select
          only if (pc[0] == 2) then
             -- action_0_2
             I[0] := (I[0] + L[0]) mod 5;
             attr (id, 0 of Nat, (I[0] + L[0]) mod 5);
             I[1] := (I[1] + L[1]) mod 5;
             attr (id, 1 of Nat, (I[1] + L[1]) mod 5);
             Zqry := insert (TUPLESTART (0), Zqry);
             Zqry := insert (TUPLESTART (1), Zqry);
             pc[0] := 2
          end if
          -- end select
        end if;
        tick(t)
      end if
      []
         -- propagate: sender side
         only if length (Zput) > 0 then
            var key : Nat in
               key := any Nat where (key < 2) and member (key, Zput);
               put (id, key, L, I); -- sync with get
               Zput := remove (key, Zput)
            end var
         end if
      []
         -- propagate: receiver side
         var sender : ID, key : Nat, k : nat, recL : Lstig, recI : Iface, cmp:Comparison in
            recL := Lstig(0);
            recI := Iface(0);
            goodLstig(!?recL);
            goodIface(!?recI);
            select
               get1 (?sender, ?key, recL, recI) where ((Nat(sender) < MAXCOMPONENTS) and (key < 2))  -- sync with put
            []
               get2 (?sender, ?key, recL, recI)  where ((Nat(sender) < MAXCOMPONENTS) and (key < 2))  -- sync with put
            end select;
            use sender;
            if link (I, recI) then
              request(key, sender, id, ?cmp);
              if (cmp == GREATER) then
                for k := key while k <= TUPLEEND (key) by k := k + 1 loop
                  if L[k] <> recL[k] then --20210428
                    L[k] := recL[k];
                    l (id, k, L[k], "from", sender)
                  else i --20210428
                  end if --20210428
                end loop;

                Zput := insert (key, Zput);
                Zqry := remove (key, Zqry)
              end if
            end if
         end var
      []
         -- confirm: sender side
         only if length (Zqry) > 0 then
            var key : Nat in
              key := any Nat where (key < 2) and member (key, Zqry);
              req (id, key, L, I); -- sync with resp
              Zqry := remove (key, Zqry)
            end var
         end if
      []
         -- confirm: receiver side
         var sender : ID, key : Nat, k : nat, recL : Lstig, recI : Iface, cmp : Comparison in
            recL := Lstig(0);
            recI := Iface(0);
            goodLstig(!?recL);
            goodIface(!?recI);
            select
               resp1 (?sender, ?key, recL, recI) where ((Nat(sender) < MAXCOMPONENTS) and (key < 2))  -- sync with req
            []
               resp2 (?sender, ?key, recL, recI) where ((Nat(sender) < MAXCOMPONENTS) and (key < 2))  -- sync with req
            end select;
            use sender;
            if link (I, recI) then 
              request(key, sender, id, ?cmp);
              if (cmp != SAME) then
                Zput := insert (key, Zput);
                if cmp == GREATER then
                  for k := key while k <= TUPLEEND (key) by k := k + 1 loop
                    if L[k] <> recL[k] then --20210428
                      L[k] := recL[k];
                      l (id, k, L[k], "from", sender)
                    else i --20210428
                    end if --20210428
                  end loop;
                  Zqry := remove (key, Zqry)
                end if
              end if
            end if
         end var
      end select
    end loop
  end var
end process

process main [tick:IDchannel, debug, put0, put1, put2, req0, req1, req2, init, attr, l, refresh, request : any] is
  par refresh, request in
    MatrixStorage[refresh, request, debug]
  ||
    par
    tick -> sched [tick]
    || 
    tick, put0, put1, put2, req0, req1, req2, init ->
      agent [tick, put0, put1, put2, req0, req1, req2, init, attr, l, refresh, request] (ID (0))
    || 
    tick, put1, put2, put0, req1, req2, req0, init ->
      agent [tick, put1, put2, put0, req1, req2, req0, init, attr, l, refresh, request] (ID (1))
    ||
    tick, put2, put0, put1, req2, req0, req1, init ->
      agent [tick, put2, put0, put1, req2, req0, req1, init, attr, l, refresh, request] (ID (2))
    end par
  end par
end process

end module

