# Artifacts related to "Verifying temporal properties of stigmergic multi-agent systems using CADP"

This repository contains several artifacts used in the experimental evaluation
of "Verifying temporal properties of multi-agent systems using CADP".

## Structure of the repository

* `labs`: LAbS specification of the benchmarked systems (adapted from
  https://github.com/labs-lang/labs-examples)
* `sequential_experiments`: Artifacts related to the verification of sequential
  emulation programs
* `flock_rr_parallel`: Artifacts related to the verification of the parallel
  emulation program for the `flock_rr` system.

The two folders `sequential_experiments` and `flock_rr_parallel` contain
several `.log` files, reporting the results of our experimental evaluation
that have been summarized in the paper.

Notice that some files related to `flock_rr` and `flock` are
named `boids_aw_fair` and `boids_aw`, respectively.

## Replicating the experiments

CADP (https://cadp.inria.fr) is needed to replicate our experiments.
Our files should be compatible with any version newer than or equal to
CADP 2021-e. 

```sh
cd sequential_experiments
svl formation_flock.svl
svl leader.svl
svl twophase.svl

cd ../flock_rr_parallel
svl rootleaf_divbranching.svl
```

Notice that the `flock_rr_parallel` folder also contains a `rootleaf_strong`
SVL file. This script performs the same verification task as
`rootleaf_divbranching` but uses strong reduction instead of divbranching
reduction, with similar results.

## Links and references

* Emulation programs were generated with SLiVER, available on GitHub ([link](github.com/labs-lang/sliver/)).

* Additional LAbS examples are available in a separate repository ([link](https://github.com/labs-lang/labs-examples)).

References on LAbS:

[1] R. De Nicola, L. Di Stefano, and O. Inverso, “Multi-agent systems with virtual stigmergy,” Sci. Comput. Program., vol. 187, p. 102345, 2020. [Link](https://doi.org/10.1016/j.scico.2019.102345)

[2] L. Di Stefano, F. Lang, and W. Serwe, “Combining SLiVER with CADP to Analyze Multi-agent Systems,” in 22nd International Conference on Coordination Models and Languages (COORDINATION). LNCS, vol. 12134. Springer, 2020. [Link](https://doi.org/10.1007/978-3-030-50029-0_23)

[3] L. Di Stefano, “Modelling and Verification of Multi-Agent Systems via Sequential Emulation,” PhD Thesis, Gran Sasso Science Institute, L’Aquila, Italy, 2020. [Link](https://iris.gssi.it/handle/20.500.12571/10181)
