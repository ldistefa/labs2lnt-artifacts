module BOIDS_CAW is
--module BOIDS_CAW_0_UNFAIR_BIRDS3GRID5DELTA5 is

    function MAXCOMPONENTS:Nat is return 3 end function
    function undef_value:Int is return -128 end function

    type ID is X:Nat where X < MAXCOMPONENTS with ==, != end type
    type PC is array [ 0 .. 0 ] of Nat end type
    type Iface is array [ 0 .. 1 ] of Int with get, set end type


    (* Stigmergy *)
    type Lstig is array [ 0 .. 5 ] of Int with get, set end type
    type Pending is array [ 0 .. 5 ] of Bool with get, set end type

    function TUPLESTART(n:Nat): Nat is
        case n in
             0 -> return 0
            | 1 -> return 0
            | 2 -> return 0
            | 3 -> return 0
            | 4 -> return 4
            | 5 -> return 4
            | any -> raise unexpected (* should never match *)
        end case
    end function

    function TUPLEEND(n:Nat): Nat is
        case n in
             (0) -> return 3
            | (1) -> return 3
            | (2) -> return 3
            | (3) -> return 3
            | (4) -> return 5
            | (5) -> return 5
            | any -> raise unexpected (* should never match *)
        end case
    end function

    function length(p:Pending): Nat is
        var n, j:Nat in
            n := 0;
            for j := 0 while j < 6 by j := j + 1 loop
                if (p[j]) then n := n + 1 end if
            end loop;
            return n
        end var
    end function

    function empty(p:Pending): Bool is
        return (length(p) == 0)
    end function

    function insert(k: Nat, p:Pending): Pending is
        var p1 : Pending in
            p1 := p;
            p1[k] := true;
            return p1
        end var
    end function

    function remove(k: Nat, p:Pending): Pending is
        var p1 : Pending in
            p1 := p;
            p1[k] := false;
            return p1
        end var
    end function

    function member(k: Nat, p:Pending): Bool is
        return p[k]
    end function


    type SetOfAgents is array [ 0 .. 2 ] of Bool with get, set end type

    function tri(n:Nat): Nat is
        (* returns the n-th triangular number *)
        return (n * (n + 1)) div 2
    end function

    type Comparison is LESS, SAME, GREATER with ==, != end type

    function inv(c:Comparison): Comparison is
        case c in
        LESS -> return GREATER
        | GREATER -> return LESS
        | SAME -> return SAME
        end case 
    end function

    -- n*(n+1) / 2
    type MatrixVar is array [0 .. 2] of Comparison with get, set end type
    -- one Matrix2D for each variable
    type Matrix is array [0 .. 5] of MatrixVar with get, set end type

    function index(i,j:Nat): Nat is
        require j < MAXCOMPONENTS;
        require i < j;
        -- I don't know why, but writing the formula in one line, i.e.,
        -- return (MAXCOMPONENTS * i) + j - tri(i+1)
        -- leads to "out of range" errors from the LOTOS compiler
        var x:Nat in
            x := tri(i+1);
            x := (MAXCOMPONENTS * i) + j - x;
            return x
        end var
    end function

    --getComparison(m,i,j) = GREATER iff i's value is fresher than j's
    function getComparison(m:Matrix, key,i,j:Nat): Comparison is
        if (i==j) then return SAME
        elsif (i<j) then return m[key][index(i,j)]
        else return inv(m[key][index(j,i)])
        end if
    end function

    function refresh(in out m: Matrix, key, id: Nat) is
        var i:Nat, mkey:MatrixVar in 
            mkey := m[key];
            for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
                if (id<i) then mkey[index(id, i)] := GREATER
                elsif (id>i) then mkey[index(i, id)] := LESS
                -- ignore the case from=i
                end if
            end loop;
            m[key] := mkey
        end var
    end function

    function sync(in out m: Matrix, key, from: Nat, to: SetOfAgents) is
        var i, j:Nat, mkey: MatrixVar in 
            mkey := m[key];
            for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
            (* Sync receiver with sender *)
                if to[i] then
                    if (from<i) then mkey[index(from, i)] := SAME
                    elsif (from>i) then mkey[index(i, from)] := SAME
                    -- ignore the case from=i
                    end if;
                    for j := 0 while j < i by j := j + 1 loop
                    (* Sync receivers with each other *)
                        if to[j] then mkey[index(j, i)] := SAME end if
                    end loop
                end if
            end loop;
            m[key] := mkey
        end var
    end function


    type Agent is agent(id: ID, I: Iface, L: Lstig, Zput: Pending, Zqry: Pending, pc:PC) with get, set end type
    type Agents is array [ 0 .. 2 ] of Agent with get, set end type

    type Sys is sys(agents: Agents, m: Matrix) with get, set end type


    function emptyAgent: Agent is
        return Agent(ID(0), Iface(0), Lstig(0), Pending(false), Pending(false), PC(0))
    end function

    process attr [attrGate: any] (in out a:Agent, key:Nat, val:Int) is 
        var Inew: Iface in 
            Inew := a.I;
            Inew[key] := val;
            a := a.{I -> Inew}
        end var;
        attrGate(a.id, key, val)
    end process

    function incr(x: Nat): Nat is 
        if x < 255 then return x + 1 else return 0 end if
    end function
    
    function link(a1: Agent, a2: Agent, key: Nat):Bool is
        
        if ((key >= 4) and (key <= 5)) then
            return (((((a1.I[IntToNat(0)]) - (a2.I[IntToNat(0)])) * ((a1.I[IntToNat(0)]) - (a2.I[IntToNat(0)]))) + (((a1.I[IntToNat(1)]) - (a2.I[IntToNat(1)])) * ((a1.I[IntToNat(1)]) - (a2.I[IntToNat(1)])))) <= (((5 of Int)) * ((5 of Int))))
        
        elsif ((key >= 0) and (key <= 3)) then
            return ((a1.L[IntToNat(0)]) >= (a2.L[IntToNat(0)]))
        
        else return false
        end if
        
    end function

    process lstig [l: any] (in out a:Agent, key:Nat, val:Int, in out m:Matrix) is 
        var Lnew: Lstig in 
            Lnew := a.L;
            Lnew[key] := val;
            eval refresh(!?m, key, Nat(a.Id));
            a := a.{L -> Lnew, Zput -> insert(TUPLESTART(key), a.Zput)};
            l(a.id, key, val)
        end var
    end process

    process propagate [l: any] (in out sys: Sys) is
        var senderId, key: Nat, sender:Agent, agents:Agents in
            senderId := any Nat where (senderId < MAXCOMPONENTS) and (length(sys.agents[senderId].Zput) > 0);
            agents := sys.agents;
            sender := agents[senderId];
            key := any Nat where key < 6 and member(key, sender.Zput);
            --action("propagate", senderId, Nat(key));
            var j: Nat, a:Agent, mat:Matrix, recipients:SetOfAgents in
                --t := sys.time;
                mat := sys.m;
                recipients:= SetOfAgents(false);
                for j := 0 while j < MAXCOMPONENTS by j := j + 1 loop
                    a := agents[j];

                    if (a.id != sender.id) and link(sender, a, key) and 
                    (getComparison(mat, key, senderId, j) == GREATER) --sender's value is fresher
                    then
                        var L:Lstig, k:Nat in
                            L := a.L;
                            for k := key while k <= TUPLEEND(key) by k := k + 1 loop
                                L[k] := sender.L[k];
                                l(j, k, L[k], "from", senderId)
                            end loop;
                            agents[j] := a.{
                                L -> L, 
                                Zput -> insert(key, a.Zput),
                                Zqry -> remove(key, a.Zqry)
                            }
                        end var;
                        recipients[j] := true
                    end if
                end loop;
                eval sync(!?mat, key, senderId, recipients);
                agents[senderId] := sender.{Zput -> remove(key, sender.Zput)};
                sys := sys.{agents -> agents, m -> mat}
            end var
        end var
    end process

    process confirm [l: any] (in out sys:Sys) is
        var senderId, key: Nat, sender:Agent, agents:Agents in
            senderId := any Nat where (senderId < MAXCOMPONENTS) and (length(sys.agents[senderId].Zqry) > 0);
            agents := sys.agents;
            sender := agents[senderId];
            key := any Nat where key < 6 and member(key, sender.Zqry);
            --action("confirm", senderId, Nat(key));
            var j: Nat, a:Agent, mat:Matrix, recipients:SetOfAgents in
                mat := sys.m;
                recipients:= SetOfAgents(false);
                for j := 0 while j < MAXCOMPONENTS by j := j + 1 loop
                    a := agents[j];
                    if (a.id != sender.id) and link(sender, a, key) and
                    (getComparison(mat, key, senderId, j) != SAME)
                    then 
                        a := a.{Zput -> insert(key, a.Zput)};
                        if 
                        (getComparison(mat, key, senderId, j) == GREATER) --sender's value is fresher
                        then
                            var L:Lstig, k:Nat in
                                L := a.L;
                                for k := key while k <= TUPLEEND(key) by k := k + 1 loop
                                    L[k] := sender.L[k];
                                    l(j, k, L[k], "from", senderId)
                                end loop;
                                agents[j] := a.{L -> L, Zqry -> remove(key, a.Zqry)}
                            end var;
                            recipients[j] := true
                        end if
                    end if
                end loop;
                eval sync(!?mat, key, senderId, recipients);
                --l(recipients, mat); --DEBUG
                agents[senderId] := sender.{Zqry -> remove(key, sender.Zqry)};
                sys := sys.{agents -> agents, m -> mat}
            end var
        end var
    end process

process INITAGENT[attrGate, l: any] (in out a:Agent, in out m:Matrix) is
    var I:Iface, x: Int, p:PC, L:Lstig in
        I := a.I;
        L := a.L;
        p := a.pc;

        if (Nat(a.id) >= 0) and (Nat(a.id) < 3) then

        p[0] := 8;


        x := any Int where (((x) == ((1 of Int))));
        L[0] := x;
        l(Nat(a.id), 0 of Nat , x);
        eval refresh(!?m, 0, Nat(a.Id));
        x := any Int where (((x) == (NatToInt(Nat(a.id)))));
        L[1] := x;
        l(Nat(a.id), 1 of Nat , x);
        eval refresh(!?m, 1, Nat(a.Id));
        x := any Int where (((x) == (-((1 of Int)))));
        L[2] := x;
        l(Nat(a.id), 2 of Nat , x);
        eval refresh(!?m, 2, Nat(a.Id));
        x := any Int where (((x) == (-((1 of Int)))));
        L[3] := x;
        l(Nat(a.id), 3 of Nat , x);
        eval refresh(!?m, 3, Nat(a.Id));
        x := any Int where (((x) == (-((1 of Int))))) or (((x) == ((1 of Int))));
        L[4] := x;
        l(Nat(a.id), 4 of Nat , x);
        eval refresh(!?m, 4, Nat(a.Id));
        x := any Int where (((x) == (-((1 of Int))))) or (((x) == ((1 of Int))));
        L[5] := x;
        l(Nat(a.id), 5 of Nat , x);
        eval refresh(!?m, 5, Nat(a.Id));
        x := any Int where (((x) >= ((0 of Int)))) and (((x) < ((5 of Int))));
        I[0] := x;
        attrGate(Nat(a.id), 0 of Nat , x);
        x := any Int where (((x) >= ((0 of Int)))) and (((x) < ((5 of Int))));
        I[1] := x;
        attrGate(Nat(a.id), 1 of Nat , x)        
        end if;

        a := a.{I -> I, L -> L, pc -> p}
    end var 
end process


process action_0_2 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --((leader, 1)) == (id)->(posX, 2),(posY, 3) <~ (x, 0),(y, 1)

    lstig[l](!?agent, 2, agent.I[IntToNat(0)], !?m);
    lstig[l](!?agent, 3, agent.I[IntToNat(1)], !?m);

    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(1), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        p[0] := 8;
        agent := agent.{pc -> p}
    end var
end process

process action_0_3 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --(abs((y, 1) - (posY, 3))) > (5)->(dirY, 5) <~ (posY, 3) - (y, 1) / abs((posY, 3) - (y, 1))

    lstig[l](!?agent, 5, ((agent.L[IntToNat(3)]) - (agent.I[IntToNat(1)])) div (abs((agent.L[IntToNat(3)]) - (agent.I[IntToNat(1)]))), !?m);

    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(3), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        p[0] := 8;
        agent := agent.{pc -> p}
    end var
end process

process action_0_4 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --(abs((y, 1) - (posY, 3))) <= (5)->√


    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(3), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        p[0] := 8;
        agent := agent.{pc -> p}
    end var
end process

process action_0_5 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --(abs((x, 0) - (posX, 2))) > (5)->(dirX, 4) <~ (posX, 2) - (x, 0) / abs((posX, 2) - (x, 0))

    lstig[l](!?agent, 4, ((agent.L[IntToNat(2)]) - (agent.I[IntToNat(0)])) div (abs((agent.L[IntToNat(2)]) - (agent.I[IntToNat(0)]))), !?m);

    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(2), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        var x: Nat in
            x := any Nat where ((x == 3) or (x == 4));
            p[0] := x
        end var;
        agent := agent.{pc -> p}
    end var
end process

process action_0_6 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --(abs((x, 0) - (posX, 2))) <= (5)->√


    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(2), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        var x: Nat in
            x := any Nat where ((x == 3) or (x == 4));
            p[0] := x
        end var;
        agent := agent.{pc -> p}
    end var
end process

process action_0_7 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --((leader, 1)) != (id)->(count, 0) <~ (count, 0) + 1

    lstig[l](!?agent, 0, (agent.L[IntToNat(0)]) + ((1 of Int)), !?m);

    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(0), Zqry);
        Zqry := insert(TUPLESTART(1), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        var x: Nat in
            x := any Nat where ((x == 5) or (x == 6));
            p[0] := x
        end var;
        agent := agent.{pc -> p}
    end var
end process

process action_0_8 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --(x, 0),(y, 1) <- (x, 0) + (dirX, 4) % 5,(y, 1) + (dirY, 5) % 5

    attr[attrGate](!?agent, 0, ((agent.I[IntToNat(0)]) + (agent.L[IntToNat(4)])) mod ((5 of Int)));
    attr[attrGate](!?agent, 1, ((agent.I[IntToNat(1)]) + (agent.L[IntToNat(5)])) mod ((5 of Int)));

    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(4), Zqry);
        Zqry := insert(TUPLESTART(5), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        var x: Nat in
            x := any Nat where ((x == 2) or (x == 7));
            p[0] := x
        end var;
        agent := agent.{pc -> p}
    end var
end process

function canProceed(a:Agent): Bool is
    var tid: ID in
    tid := a.id;
    return (empty(a.Zput) and empty(a.Zqry)) and 
        (        ((a.pc[0] == 2) and ((a.L[IntToNat(1)]) == ((NatToInt(Nat(tid)) of Int))))
        or        ((a.pc[0] == 3) and ((abs((a.I[IntToNat(1)]) - (a.L[IntToNat(3)]))) > ((5 of Int))))
        or        ((a.pc[0] == 4) and ((abs((a.I[IntToNat(1)]) - (a.L[IntToNat(3)]))) <= ((5 of Int))))
        or        ((a.pc[0] == 5) and ((abs((a.I[IntToNat(0)]) - (a.L[IntToNat(2)]))) > ((5 of Int))))
        or        ((a.pc[0] == 6) and ((abs((a.I[IntToNat(0)]) - (a.L[IntToNat(2)]))) <= ((5 of Int))))
        or        ((a.pc[0] == 7) and ((a.L[IntToNat(1)]) != ((NatToInt(Nat(tid)) of Int))))
        or        ((a.pc[0] == 8))
        )
    end var
end function

function existsEnabled(agents:Agents): Bool is
    var i:Nat in
        for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
            if canProceed(agents[i]) then return true end if
        end loop
    end var;
    return false
end function

process step [attrGate, l: any, spurious:None] (in out sys: Sys, tid: ID) is
    var a:Agent, agents:Agents, mat: Matrix, firstAgent: Int in
        agents := sys.agents;
        mat := sys.m;
        --last(?tid);
        a := agents[Nat(tid)];
        firstAgent := NatToInt(Nat(tid));
        

        --select
        if (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 2) and ((a.L[IntToNat(1)]) == ((NatToInt(Nat(tid)) of Int)))) then action_0_2[attrGate, l](!?a, !?mat)
        elsif (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 3) and ((abs((a.I[IntToNat(1)]) - (a.L[IntToNat(3)]))) > ((5 of Int)))) then action_0_3[attrGate, l](!?a, !?mat)
        elsif (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 4) and ((abs((a.I[IntToNat(1)]) - (a.L[IntToNat(3)]))) <= ((5 of Int)))) then action_0_4[attrGate, l](!?a, !?mat)
        elsif (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 5) and ((abs((a.I[IntToNat(0)]) - (a.L[IntToNat(2)]))) > ((5 of Int)))) then action_0_5[attrGate, l](!?a, !?mat)
        elsif (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 6) and ((abs((a.I[IntToNat(0)]) - (a.L[IntToNat(2)]))) <= ((5 of Int)))) then action_0_6[attrGate, l](!?a, !?mat)
        elsif (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 7) and ((a.L[IntToNat(1)]) != ((NatToInt(Nat(tid)) of Int)))) then action_0_7[attrGate, l](!?a, !?mat)
        elsif (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 8)) then action_0_8[attrGate, l](!?a, !?mat)
        else spurious; stop
        end if;
        --end select;
    
        agents[Nat(tid)] := a;
        sys := sys.{ agents -> agents, m -> mat}

    end var
end process

process MAIN [attr, l: any, spurious:None] is
    var sys: Sys, tid: ID in
        var agents: Agents, m:Matrix in
            agents := Agents(emptyAgent);
            m := Matrix(MatrixVar(SAME));        
            var i: Nat, a: Agent in
                for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
                    a := agents[i].{id -> ID(i)};
                    INITAGENT[attr, l](!?a, !?m);
                    agents[i] := a--;
                    --action(a)
                end loop;
                sys := Sys(agents, m)
            end var
        end var;

        -- select 1st agent
        tid :=  0;

        loop
            --monitor[monitor](sys.agents);
            
            select
                    --- scheduler ---
                tid := any ID;
                --- end scheduler ---
                step[attr, l, spurious](!?sys, tid)
            []
                propagate [l] (!?sys)
            []
                confirm [l] (!?sys)
            end select
        end loop
    end var
end process

end module


