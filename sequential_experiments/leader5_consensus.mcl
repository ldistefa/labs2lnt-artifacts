(*
This schema should allow to verify arbitrary state-based predicates over the
states of agents.
*)

macro Predicate(v0, v1, v2, v3, v4) =
 (**
 L[0] is 0 for all agents
 *)
 (v0 = 0) and (v1 = 0) and (v2 = 0) and (v3 = 0) and (v4 = 900)
end_macro


macro RP(v0_, v1_, v2_, v3_, v4_) =
    (* mu = ReachPredicate is a liveness property *)
    mu ReachPredicate(v0:Int := v0_, v1:Int := v1_, v2:Int := v2_, v3:Int := v3_, v4:Int := v4_) . (
        (** 
        The predicate holds
        *)
        Predicate(v0, v1, v2, v3, v4)
        or 
        (**
        We can only do a "spurious"-move. This means that we're in a state 
        that's not allowed by LAbS semantics, so we just say that it 
        vacuously satisfies the property
        *)
        ((<"SPURIOUS"> true) and ([not "SPURIOUS"] false))
        or
        (** 
            We can do a non-L, non-SPURIOUS move and reach a state where ReachPredicate holds 
        *)
        < not {L ... } and not "SPURIOUS" > ReachPredicate(v0,v1,v2,v3,v4)
        or
        (** 
            Agent i can update L[0]: we reach a state where ReachPredicate
            holds (with updated value for vi)
        *)
        < {L ?id:Nat !0 ?v:Int ... } > (
            if (id=0) then ( ReachPredicate(v, v1, v2, v3, v4) )
            elsif (id=1) then ( ReachPredicate(v0, v, v2, v3, v4) )
            elsif (id=2) then ( ReachPredicate(v0, v1, v, v3, v4) )
            elsif (id=3) then ( ReachPredicate(v0, v1, v2, v, v4) )
            elsif (id=4) then ( ReachPredicate(v0, v1, v2, v3, v) )
            else false
            end if
        )
    )
end_macro

(* Capture initial values of L[0] for all agents *)
[
    (not {L ...}) * . {L !0 !0 ?init0:Int ...} .
    (not {L ...}) * . {L !1 !0 ?init1:Int ...} .
    (not {L ...}) * . {L !2 !0 ?init2:Int ...} .
    (not {L ...}) * . {L !3 !0 ?init3:Int ...} .
    (not {L ...}) * . {L !4 !0 ?init4:Int ...}
] 
(
(* nu = FairPredicate is an invariant *)
nu FairPredicate (x0:Int := init0, x1:Int := init1, x2:Int := init2, x3:Int := init3, x4:Int := init4) . (
    (* Satisfaction of Predicate is reachable from here *)
    RP(x0, x1, x2, x3, x4) and
    (* Satisfaction of Predicate is reachable from all successors
       (except those reached via SPURIOUS-moves)
     *)
    ([ not "SPURIOUS" and not {L ...} ] FairPredicate(x0,x1,x2,x3,x4)) and
    ([ {L ?id:Nat !0 ?x:Int ... } ] (
            if (id=0) then ( FairPredicate(x, x1, x2, x3, x4) )
            elsif (id=1) then ( FairPredicate(x0, x, x2, x3, x4) )
            elsif (id=2) then ( FairPredicate(x0, x1, x, x3, x4) )
            elsif (id=3) then ( FairPredicate(x0, x1, x2, x, x4) )
            elsif (id=4) then ( FairPredicate(x0, x1, x2, x3, x) )
            else false
            end if
        )
    )
)
)
