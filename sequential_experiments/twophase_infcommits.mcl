(*
This schema should allow to verify arbitrary state-based predicates over the
states of agents.
*)

macro Predicate(v0) =
 (**
 The variable becomes 1
 *)
 v0 = 1
end_macro


macro RP(v0_) =
    (* mu = ReachPredicate is a liveness property *)
    mu ReachPredicate(v0:Int := v0_) . (
        (** 
        The predicate holds
        *)
        Predicate(v0)
        or 
        (**
        We can only do a "spurious"-move. This means that we're in a state 
        that's not allowed by LAbS semantics, so we just say that it 
        vacuously satisfies the property
        *)
        ((<"SPURIOUS"> true) and ([not "SPURIOUS"] false))
        or
        (** 
            We can do a non-ATTR, non-SPURIOUS move and reach a state where ReachPredicate holds 
        *)
        (< not {ATTR ... } and not "SPURIOUS" > ReachPredicate(v0))
        or
        (** 
            We can do an ATTR-move and reach a state where ReachPredicate holds 
        *)
        (< {ATTR ?any !1 ... } > ReachPredicate(v0))
        or 
        (< {ATTR ?id:Nat !0 ?v:Int ... } > (
            if (id=0) then ( ReachPredicate(v) )
            else false
            end if
        ))
    )
end_macro

(* Capture initial values of I[0] for all agents *)
[
    (not {ATTR ...}) * . {ATTR !0 !0 ?init0:Int ...}
] 
(
(* nu = FairPredicate is an invariant *)
nu FairPredicate (x0:Int := init0) . (
    (* Satisfaction of Predicate is reachable from here *)
    RP(x0) and
    (* Satisfaction of Predicate is reachable from all successors
       (except those reached via SPURIOUS-moves)
     *)
    ([ not "SPURIOUS" and not {ATTR ...} ] FairPredicate(x0)) and
    ([ {ATTR ?any !1 ?x:Int ... } ]  FairPredicate(x0)) and
    ([ {ATTR ?id:Nat !0 ?x:Int ... } ] (
            if (id=0) then ( FairPredicate(x) )
            else false
            end if
        )
    )
)
)
