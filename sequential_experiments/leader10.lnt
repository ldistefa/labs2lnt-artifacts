module LEADER10 is

    function MAXCOMPONENTS:Nat is return 10 end function
    function undef_value:Int is return -128 end function

    type ID is X:Nat where X < MAXCOMPONENTS with ==, != end type
    type PC is array [ 0 .. 0 ] of Nat end type
    type Iface is array [ 0 .. 0 ] of Int with get, set end type


    (* Stigmergy *)
    type Lstig is array [ 0 .. 0 ] of Int with get, set end type
    type Pending is array [ 0 .. 0 ] of Bool with get, set end type

    function TUPLESTART(n:Nat): Nat is
        case n in
             0 -> return 0
            | any -> raise unexpected (* should never match *)
        end case
    end function

    function TUPLEEND(n:Nat): Nat is
        case n in
             (0) -> return 0
            | any -> raise unexpected (* should never match *)
        end case
    end function

    function length(p:Pending): Nat is
        var n, j:Nat in
            n := 0;
            for j := 0 while j < 1 by j := j + 1 loop
                if (p[j]) then n := n + 1 end if
            end loop;
            return n
        end var
    end function

    function empty(p:Pending): Bool is
        return (length(p) == 0)
    end function

    function insert(k: Nat, p:Pending): Pending is
        var p1 : Pending in
            p1 := p;
            p1[k] := true;
            return p1
        end var
    end function

    function remove(k: Nat, p:Pending): Pending is
        var p1 : Pending in
            p1 := p;
            p1[k] := false;
            return p1
        end var
    end function

    function member(k: Nat, p:Pending): Bool is
        return p[k]
    end function


    type SetOfAgents is array [ 0 .. 9 ] of Bool with get, set end type

    function tri(n:Nat): Nat is
        (* returns the n-th triangular number *)
        return (n * (n + 1)) div 2
    end function

    type Comparison is LESS, SAME, GREATER with ==, != end type

    function inv(c:Comparison): Comparison is
        case c in
        LESS -> return GREATER
        | GREATER -> return LESS
        | SAME -> return SAME
        end case 
    end function

    -- n*(n+1) / 2
    type MatrixVar is array [0 .. 44] of Comparison with get, set end type
    -- one Matrix2D for each variable
    type Matrix is array [0 .. 0] of MatrixVar with get, set end type

    function index(i,j:Nat): Nat is
        require j < MAXCOMPONENTS;
        require i < j;
        -- I don't know why, but writing the formula in one line, i.e.,
        -- return (MAXCOMPONENTS * i) + j - tri(i+1)
        -- leads to "out of range" errors from the LOTOS compiler
        var x:Nat in
            x := tri(i+1);
            x := (MAXCOMPONENTS * i) + j - x;
            return x
        end var
    end function

    --getComparison(m,i,j) = GREATER iff i's value is fresher than j's
    function getComparison(m:Matrix, key,i,j:Nat): Comparison is
        if (i==j) then return SAME
        elsif (i<j) then return m[key][index(i,j)]
        else return inv(m[key][index(j,i)])
        end if
    end function

    --Enforces that a future call to getComparison(..., i,j) will return "cmp"
    function setComparison(in out mkey:MatrixVar, i,j:Nat, cmp:Comparison) is
        if (i<j) then mkey[index(i,j)] := cmp
        elsif (i>j) then mkey[index(j,i)] := inv(cmp)
        -- ignore the case i=j
        end if
    end function

    function refresh(in out m: Matrix, key, id: Nat) is
        var i:Nat, mkey:MatrixVar in 
            mkey := m[TUPLESTART(key)];
            for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
                setComparison(!?mkey, id, i, GREATER)
                --if (id<i) then mkey[index(id, i)] := GREATER
                --elsif (id>i) then mkey[index(i, id)] := LESS
                
                --end if
            end loop;
            m[TUPLESTART(key)] := mkey
        end var
    end function

    function sync(in out m: Matrix, key, from: Nat, to: SetOfAgents) is
        var i, j:Nat, mkey: MatrixVar in 
            mkey := m[TUPLESTART(key)];
            for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
                if to[i] then 
                    (* Sync receiver with sender *)
                    setComparison(!?mkey, from, i, SAME);
                    for j := 0 while j < i by j := j + 1 loop
                        (* Sync receivers with each other *)
                        if to[j] then setComparison(!?mkey, i, j, SAME) end if
                    end loop
                elsif i <> from then
                    -- Update non-receivers
                    for j := 0 while j < i by j := j + 1 loop
                        if to[j]
                        (*i's comparison to receiver "j" is the same as with "from" *)
                        then setComparison(!?mkey, i, j, getComparison(m, key, i, from))
                        end if
                    end loop
                end if
                -- ignore case i = from
            end loop;
            m[TUPLESTART(key)]:= mkey
        end var
    end function


    type Agent is agent(id: ID, I: Iface, L: Lstig, Zput: Pending, Zqry: Pending, pc:PC) with get, set end type
    type Agents is array [ 0 .. 9 ] of Agent with get, set end type

    type Sys is sys(agents: Agents, m: Matrix) with get, set end type


    function emptyAgent: Agent is
        return Agent(ID(0), Iface(0), Lstig(0), Pending(false), Pending(false), PC(0))
    end function

    process attr [attrGate: any] (in out a:Agent, key:Nat, val:Int) is 
        var Inew: Iface in 
            Inew := a.I;
            Inew[key] := val;
            a := a.{I -> Inew}
        end var;
        attrGate(a.id, key, val)
    end process

    function incr(x: Nat): Nat is 
        if x < 255 then return x + 1 else return 0 end if
    end function
    
    function link(a1: Agent, a2: Agent, key: Nat):Bool is
        
        if ((key >= 0) and (key <= 0)) then
            return true
        
        else return false
        end if
        
    end function

    process lstig [l: any] (in out a:Agent, key:Nat, val:Int, in out m:Matrix) is 
        var Lnew: Lstig in 
            Lnew := a.L;
            Lnew[key] := val;
            eval refresh(!?m, key, Nat(a.Id));
            a := a.{L -> Lnew, Zput -> insert(TUPLESTART(key), a.Zput)};
            l(a.id, key, val)
        end var
    end process

    process propagate [l: any] (in out sys: Sys) is
        var senderId, key: Nat, sender:Agent, agents:Agents in
            senderId := any Nat where (senderId < MAXCOMPONENTS) and (length(sys.agents[senderId].Zput) > 0);
            agents := sys.agents;
            sender := agents[senderId];
            key := any Nat where key < 1 and member(key, sender.Zput);
            --action("propagate", senderId, Nat(key));
            var j: Nat, a:Agent, mat:Matrix, recipients:SetOfAgents in
                --t := sys.time;
                mat := sys.m;
                recipients:= SetOfAgents(false);
                for j := 0 while j < MAXCOMPONENTS by j := j + 1 loop
                    a := agents[j];

                    if (a.id != sender.id) and link(sender, a, key) and 
                    (getComparison(mat, key, senderId, j) == GREATER) --sender's value is fresher
                    then
                        var L:Lstig, k:Nat in
                            L := a.L;
                            for k := key while k <= TUPLEEND(key) by k := k + 1 loop
                                L[k] := sender.L[k];
                                l(j, k, L[k], "from", senderId)
                            end loop;
                            agents[j] := a.{
                                L -> L, 
                                Zput -> insert(key, a.Zput),
                                Zqry -> remove(key, a.Zqry)
                            }
                        end var;
                        recipients[j] := true
                    end if
                end loop;
                eval sync(!?mat, key, senderId, recipients);
                agents[senderId] := sender.{Zput -> remove(key, sender.Zput)};
                sys := sys.{agents -> agents, m -> mat}
            end var
        end var
    end process

    process confirm [l: any] (in out sys:Sys) is
        var senderId, key: Nat, sender:Agent, agents:Agents in
            senderId := any Nat where (senderId < MAXCOMPONENTS) and (length(sys.agents[senderId].Zqry) > 0);
            agents := sys.agents;
            sender := agents[senderId];
            key := any Nat where key < 1 and member(key, sender.Zqry);
            --action("confirm", senderId, Nat(key));
            var j: Nat, a:Agent, mat:Matrix, recipients:SetOfAgents in
                mat := sys.m;
                recipients:= SetOfAgents(false);
                for j := 0 while j < MAXCOMPONENTS by j := j + 1 loop
                    a := agents[j];
                    if (a.id != sender.id) and link(sender, a, key) and
                    (getComparison(mat, key, senderId, j) != SAME)
                    then 
                        a := a.{Zput -> insert(key, a.Zput)};
                        if 
                        (getComparison(mat, key, senderId, j) == GREATER) --sender's value is fresher
                        then
                            var L:Lstig, k:Nat in
                                L := a.L;
                                for k := key while k <= TUPLEEND(key) by k := k + 1 loop
                                    L[k] := sender.L[k];
                                    l(j, k, L[k], "from", senderId)
                                end loop;
                                agents[j] := a.{L -> L, Zqry -> remove(key, a.Zqry)}
                            end var;
                            recipients[j] := true
                        end if
                    end if
                end loop;
                eval sync(!?mat, key, senderId, recipients);
                --l(recipients, mat); --DEBUG
                agents[senderId] := sender.{Zqry -> remove(key, sender.Zqry)};
                sys := sys.{agents -> agents, m -> mat}
            end var
        end var
    end process

process INITAGENT[attrGate, l: any] (in out a:Agent, in out m:Matrix) is
    var I:Iface, x: Int, p:PC, L:Lstig in
        I := a.I;
        L := a.L;
        p := a.pc;

        if (Nat(a.id) >= 0) and (Nat(a.id) < 10) then

        p[0] := 2;


        x := any Int where (((x) == ((10 of Int))));
        L[0] := x;
        l(Nat(a.id), 0 of Nat , x);
        eval refresh(!?m, 0, Nat(a.Id))        
        end if;

        a := a.{I -> I, L -> L, pc -> p}
    end var 
end process


process action_0_2 [attrGate, l: any] (in out agent: Agent, in out m: Matrix) is
    --((leader, 0)) > (id)->(leader, 0) <~ id

    lstig[l](!?agent, 0, (NatToInt(Nat(agent.id)) of Int), !?m);

    var Zqry: Pending in
        Zqry := agent.Zqry;
        Zqry := insert(TUPLESTART(0), Zqry);
        agent := agent.{Zqry -> Zqry}
    end var;

    var p: PC in
        p := agent.pc;
        
        p[0] := 2;
        agent := agent.{pc -> p}
    end var
end process

function canProceed(a:Agent): Bool is
    var tid: ID in
    tid := a.id;
    return (empty(a.Zput) and empty(a.Zqry)) and 
        (        ((a.pc[0] == 2) and ((a.L[IntToNat(0)]) > ((NatToInt(Nat(tid)) of Int))))
        )
    end var
end function

function existsEnabled(agents:Agents): Bool is
    var i:Nat in
        for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
            if canProceed(agents[i]) then return true end if
        end loop
    end var;
    return false
end function

process step [attrGate, l: any, spurious:None] (in out sys: Sys, tid: ID) is
    var a:Agent, agents:Agents, mat: Matrix, firstAgent: Int in
        agents := sys.agents;
        mat := sys.m;
        --last(?tid);
        a := agents[Nat(tid)];
        firstAgent := NatToInt(Nat(tid));
        

        --select
        if (empty(a.Zput) and empty(a.Zqry)) and 
        ((a.pc[0] == 2) and ((a.L[IntToNat(0)]) > ((NatToInt(Nat(tid)) of Int)))) then action_0_2[attrGate, l](!?a, !?mat)
        else spurious; stop
        end if;
        --end select;
    
        agents[Nat(tid)] := a;
        sys := sys.{ agents -> agents, m -> mat}

    end var
end process

process MAIN [attr, l: any, spurious:None] is
    var sys: Sys, tid: ID in
        var agents: Agents, m:Matrix in
            agents := Agents(emptyAgent);
            m := Matrix(MatrixVar(SAME));        
            var i: Nat, a: Agent in
                for i := 0 while i < MAXCOMPONENTS by i := i + 1 loop
                    a := agents[i].{id -> ID(i)};
                    INITAGENT[attr, l](!?a, !?m);
                    agents[i] := a--;
                    --action(a)
                end loop;
                sys := Sys(agents, m)
            end var
        end var;

        -- select 1st agent
        tid :=  0;

        loop
            --monitor[monitor](sys.agents);
            
            select
                    --- scheduler ---
                tid := any ID;
                --- end scheduler ---
                step[attr, l, spurious](!?sys, tid)
            []
                propagate [l] (!?sys)
            []
                confirm [l] (!?sys)
            end select
        end loop
    end var
end process

end module


